package com.example.demo.dto;

import java.io.Serializable;

public class PersonneDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long num;
	private String nom;
	private String prenom;
	public Long getNum() {
		return num;
	}
	public void setNum(Long num) {
		this.num = num;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public PersonneDto(Long num, String nom, String prenom) {
		super();
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
	}
	public PersonneDto(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}
	public PersonneDto(Long num, String prenom) {
		super();
		this.num = num;
		this.prenom = prenom;
	}
	@Override
	public String toString() {
		return "PersonneDto [num=" + num + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	     

}

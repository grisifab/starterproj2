package com.example.demo.service;

import java.util.Collection;
import com.example.demo.models.Personne;

public interface PersonneService {
	
	Collection<Personne> getAllPersonne();

	Personne getPersonneById(Long id);

	Personne saveOrUpdatePersonne(Personne personne);

	void deletePersonne(Long id);


}


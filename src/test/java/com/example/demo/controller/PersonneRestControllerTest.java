package com.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito.BDDMyOngoingStubbing;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.demo.dao.PersonneRepository;
import com.example.demo.models.Personne;
import com.example.demo.service.PersonneService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonneRestController.class)
class PersonneRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PersonneService personneService;

	Personne personne1 = new Personne("Le Bellocu", "Yvanu");

	@Test // on teste uniquement la couche du haut, sans jamais descendre vers la BDD !
	void testGetPersonnes() throws Exception {
		Personne mockPersonne1 = new Personne("Le Belloc", "Yvan");
		Personne mockPersonne2 = new Personne("Le Belloco", "Yvano");		
		List<Personne> personneList = new ArrayList<>();
		personneList.add(mockPersonne1);
		personneList.add(mockPersonne2);
		
		List<Personne> personneList2 = new ArrayList<>();
		personneList.add(personne1);
		personneList.add(personne1);
		
		Mockito.when(personneService.getAllPersonne()).thenReturn(personneList);
		
		String URI = "/api/v1/personnes";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expectedJson = this.mapToJson(personneList);
		//String expectedJson = this.mapToJson(personneList2); pour mettre en défaut
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);

	}

	@Test
	void testGetPersonneById() throws Exception {

		Personne mockPersonne = new Personne("admin", "admin");

		Mockito.when(personneService.getPersonneById(Mockito.anyLong())).thenReturn(mockPersonne);

		String URI = "/api/v1/personnes/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		//String expectedJson = this.mapToJson(personne1); // pour mettre en défaut
		String expectedJson = this.mapToJson(mockPersonne);
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);
	}

	@Test
	void testSave() throws Exception {

		Personne mockPersonne = new Personne("admin", "admin");

		String inputInJson = this.mapToJson(mockPersonne);

		String URI = "/api/v1/personnes";

		Mockito.when(personneService.saveOrUpdatePersonne(Mockito.any(Personne.class))).thenReturn(mockPersonne);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		String outputInJson = response.getContentAsString();

		assertThat(outputInJson).isEqualTo(inputInJson);

	}

	@Test
	void testUpdatePersonne() throws Exception {
		
		Personne mockPersonne = new Personne(1L, "admin", "admin");
		
		Mockito.when(personneService.getPersonneById(Mockito.anyLong())).thenReturn(mockPersonne);
					      
		Personne personneFromDB = personneService.getPersonneById(mockPersonne.getNum());
		
		personneFromDB.setNom("admino");	

		String inputInJson = this.mapToJson(personneFromDB);

		String URI = "/api/v1/personnes/1";

		Mockito.when(personneService.saveOrUpdatePersonne(Mockito.any(Personne.class))).thenReturn(mockPersonne);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		String outputInJson = response.getContentAsString();

		assertThat(outputInJson).isEqualTo(inputInJson);
	}

	@Test
	void testDeletePersonne() throws Exception {
		Personne  mockPersonne = new Personne(1L, "admin", "admin");
		Mockito.when(personneService.getPersonneById(Mockito.anyLong())).thenReturn(mockPersonne);
		
		doNothing().when(personneService).deletePersonne(mockPersonne.getNum());
		
		
		String URI = "/api/v1/personnes/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(URI).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		
		verify(personneService, times(1)).getPersonneById(mockPersonne.getNum());
		verify(personneService, times(1)).deletePersonne(mockPersonne.getNum());

		

	}

	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}

}
